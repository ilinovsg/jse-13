package ru.ilinovsg.tm.controller;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.entity.User;
import ru.ilinovsg.tm.enumerated.Role;
import ru.ilinovsg.tm.service.UserProjectTaskService;
import ru.ilinovsg.tm.service.UserService;
import ru.ilinovsg.tm.utils.hashMD5;

import java.util.List;

public class UserController extends AbstractController {

    public Long curUserId = null;

    private final UserService userService;

    private final UserProjectTaskService userProjectTaskService;

    public UserController(UserService userService, UserProjectTaskService userProjectTaskService) {
        this.userService = userService;
        this.userProjectTaskService = userProjectTaskService;
    }

    public int createUser() {
        System.out.println("[Create user]");
        System.out.println("[Please, enter login]");
        final String login = scanner.nextLine();
        System.out.println("[Please, enter password]");
        final String password = scanner.nextLine();
        System.out.println("[Please, enter firstName]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter lastName]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter access role: Admin or User]");
        final String role = scanner.nextLine();
        userService.create(login, password, firstName, lastName, Role.valueOf(role));
        System.out.println("[OK]");
        return 0;
    }

    public int clearUser() {
        System.out.println("[Clear user]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[View user]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("[OK]");
    }

    public int viewUserById() {
        System.out.println("Enter user id");
        final Long id = scanner.nextLong();
        final User user = userService.findById(id);
        viewUser(user);
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("Enter user login");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    public int listUser() {
        System.out.println("[List user]");
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user : users) {
            System.out.println(index + "." + user.getId() + " " + user.getLogin() + " role: " + user.getRole());
            index++;
        }
    }

    public int updateUserById() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user id]");
        final Long id = Long.parseLong(scanner.nextLine());
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter user login]");
        final String login = scanner.nextLine();
        System.out.println("[Please, enter user password]");
        final String password = scanner.nextLine();
        System.out.println("[Please, enter user first name]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user last name]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user role (Admin or User)]");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, password, firstName, lastName, Role.valueOf(role));
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user login]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter user password]");
        final String password = scanner.nextLine();
        System.out.println("[Please, enter user first name]");
        final String firstName = scanner.nextLine();
        System.out.println("[Please, enter user last name]");
        final String lastName = scanner.nextLine();
        System.out.println("[Please, enter user role (Admin or User)]");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, password, firstName, lastName, Role.valueOf(role));
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[Please, enter user id]");
        final Long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[Please, enter user login]");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int getCurrentUser() {
        final User user = userService.findById(curUserId);
        if (user == null) System.out.println("[FAIL]");
        viewUser(user);
        return 0;
    }

    public int signInUser() {
        System.out.println("[Please, enter user login]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        System.out.println("[Please, enter user password]");
        final String password = scanner.nextLine();
        if (user == null || (!hashMD5.md5(password).equals(user.getPassword()))) System.out.println("[FAIL]");
        else {
            System.out.println("[OK]");
            curUserId = user.getId();
        }
        return 0;
    }

    public int signOutUser() {
        System.out.println("[OK]");
        curUserId = null;
        return 0;
    }

    public int updateUserPassword() {
        final User user = userService.findById(curUserId);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Update current password]");
        System.out.println("[Please, enter new password]");
        final String newPassword = scanner.nextLine();
        System.out.println("[Please, enter confirm password]");
        final String confirmPassword = scanner.nextLine();
        if (newPassword.equals(confirmPassword)) {
            userService.update(user.getId(), newPassword);
            System.out.println("[OK]");
        }
        else System.out.println("[FAIL]");
        return 0;
    }

    public int listProjectByCurrentUser() {
        final User user = userService.findById(curUserId);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        List<Project> projects = userProjectTaskService.findAllProjectsByUserId(curUserId);
        viewProjects(projects);
        System.out.println("[OK]");
        return 0;
    }

    public void viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) return;
        int index = 1;
        for(final Project project: projects) {
            if (project.getUserId() == null)
                System.out.println(index + "." + project.getId() + " " + project.getName());
            else
                System.out.println(index + "." + project.getId() + " " + project.getName() + " " + project.getUserId());
            index++;
        }
    }

    public int listTaskByCurrentUser() {
        final User user = userService.findById(curUserId);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        List<Task> tasks = userProjectTaskService.findAllTasksByUserId(curUserId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for(final Task task: tasks) {
            if (task.getUserId() == null)
                System.out.println(index + "." + task.getId() + " " + task.getName());
            else
                System.out.println(index + "." + task.getId() + " " + task.getName() + " " + task.getUserId());
            index++;
        }
    }

}
